import sys

'''
Funkce pro zpracovani chyb, ke kterym doslo v prubehu provadeni scriptu. VYpise text chyby do stderr a ukonci script s
navratovym kodem, uvedenym ve specifikace

@:param String text - popis chyby (TYPE_ERROR : popis)
        int error_code - kod ukonceni scriptu
@:return void
'''


def error_print(text, error_code):
    if error_code == 1:  # Argument error
        print(text, file=sys.stderr)
    if error_code == 2:  # Input error
        print(text, file=sys.stderr)
    if error_code == 4:  # Format file error
        print(text, file=sys.stderr)

    sys.exit(error_code)
