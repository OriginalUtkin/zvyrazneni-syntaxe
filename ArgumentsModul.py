import sys
import getopt
from GlobalModul import Globals

from HelpModul import helpPrint
from ErrorModul import error_print


'''
Funkce <work_with_arguments> provadi zpracovani vstupnich argumentu ze stdin : pocet argumentu, spravnost  
zapisu jednotlyvych argumentu a platnost jejich parametru.V pripade nalezeni neplatneho argumentu funkce vola 
method print_error s kodem cislo 1.

@:param  argumets - je promenna tzpu <tuple>, ktera obsahuje vstupni argumenty scriptu 
@:return void 
'''


def work_with_arguments(script_arguments):

    # Kontrola vsech vstupnich argumentu, obsahujicich na zacatku '--'
    try:
        opts, args = getopt.getopt(script_arguments, '', Globals.validArguments) # Kontrola vstupnich argumemtu
    except getopt.GetoptError:  # Nalezen neznamy argument -> chyba cislo (1)
        error_print('ARGUMENT ERROR : Nalezen neznamy argument programu\n', 1)

    # Pokud vstupni argument neobsahuje '--' je nutne ukoncit script s kodem cislo 1
    for key in script_arguments:
        if key[0:2] == "--":
            continue
        if key not in Globals.validArguments:
            error_print('ARGUMENT ERROR : Nespravny format vstupniho argumentu\n', 1)

    # Vsichni vstupni argumenty jsou spravne -> provadi zpracovani kazdeho argumentu
    for opt, arg in opts:  # Vsichni argumenty jsou zadane spravne, je nutne dalsi zpracovani

        # Zpracovani argumentu '--help'
        if opt == '--help':
            if(sys.argv.__len__() == 2):  # Zadan pouze '--help' argument
                helpPrint()
            else:   # Pokud existuje --help argument, ale zadan nejaky dalsi
                error_print('ARGUMENT ERROR : Argument help nelze kombinovat s ostatnimi argumenty\n', 1)

        # Zpracovani argumentu '--input'
        elif opt == '--input':
            try:  # Pokus otevreni formatovaciho souboru
                Globals.inputFile = open(arg, "r", encoding="utf-8")
            except IOError:  # Argument --input ukazuje na soubor, ktery nejade otevrit nebo neexistuje
                error_print('INPUT ERROR : Spatny vstupni soubor nebo soubor neexistuje\n', 2)
            Globals.inputFileVar = True

        # Zpracovani argumentu '--output'
        elif opt == '--output':
            Globals.outputFile = open(arg, "w", encoding="utf-8")
            Globals.outputVar = True

        # Zpracovani argumentu '--format'
        elif opt == '--format':
            try:  # Pokus otevreni formatovaciho souboru
                Globals.inputFormat = open(arg, "r", encoding="utf-8")
            except IOError:
                error_print('FORMAT ERROR : Spatny vstupni formatovaci soubor nebo soubor neexistuje\n', 2)
            Globals.inputFormatVar = True

        # Zpracovani argumentu '--br'
        else:
            Globals.br = True

    # Nenalezen argument, obsahujici cestu do vstupniho souboru
    if not Globals.inputFileVar :
        error_print('ARGUMENT ERROR : Nenalezen argument --input\n', 2)
