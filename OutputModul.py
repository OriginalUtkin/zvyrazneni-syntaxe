import re
import sys
from GlobalModul import Globals


'''
Funkce vypocita pozice pro umisteni otevirajiciho a uzavirajiciho tagu pro uvedene pravidlo ze formatovaciho souboru.
Pomoci iteratoru zajistuje zacatek a konec vyskytu retezce ve vstupnim souboru a pak vypocita pozice pro startovaci a 
ukoncovaci tagy. Pokud pred tagem existuje nejaky jiny tag, pak posouva tag o delku predchozich tagu

@:param String input_file - retezec, obsahujici nacteny text ze vstupniho souboru
@:return list position_list - seznam, obsahujici nazev tagu a ho pozici ve vystupnim souboru
'''


def get_position(input_file):
    # Seznam, obsahujici index pro kazdy tag ze seznamu, obsahujiciho vsichni pravidla
    position_list = []

    for rule in Globals.format_file:
        # Pripraveni iteratoru objektu na zaklade regularnigo vyrazu
        reg_from_rule = rule[0]
        search_iterator = re.finditer(reg_from_rule, input_file, re.DOTALL)

        for match in search_iterator:
            # Zajisteni indexu zacatku vyskytu a koncoveho indexu pro soucasny regularni vyraz (0 - zacatek/1 - koncovy)
            position = match.span()

            # index pro otevirajici tag (neobsahuje konecny index!!!)
            position_start = position[0]

            # Zajisteni offsetu pro otevirajici tag
            for position_other in position_list:
                # Pokud pred otevirajici tag vyskytne neco -> posun o delku tagu ktery je drive
                if position_start >= position_other[1]:
                    position_start = position_start + len(position_other[0])
                else:
                    continue
            # ulozeni pozice otevirajiciho tagu do seznamu
            position_list.append((rule[1][0], position_start))

            # index pro uzavirajici tag (neobsahuje konecny index!!!)
            position_end = position[1]

            # Zajisteni offsetu pro uzavirajici tag
            for position_other in position_list:
                # Pokud pred uzaviriajici tag vyskytne neco -> posun o delku tagu
                if position_end > position_other[1]:
                    position_end = position_end + len(position_other[0])
                else:
                    continue
            # ulozeni pozice uzavirajiciho tagu, odpovidajici pro predchozi tag do seznamu
            position_list.append((rule[1][1], position_end))

    return position_list


'''
Funkce pripravuje vystupni soubor na zaklade seznamu, obsahujiciho umisteni kezdeho tagu

@:param list positional_list - seznam, obsahujici pozice kazdeho tagu ve vystupnim souboru (+ offset)
@:param String input_file - obsah vstupniho souboru ktery je nutne zpracovat na zaklade positional_list
@:return String final_output - zpracovany vstup na zaklade formatovaciho souboru
'''


def get_output(positional_list, input_file):

    for tag_position in positional_list:
       input_file = input_file[:tag_position[1]] + tag_position[0] + input_file[tag_position[1]:]

    return input_file


'''
Hlavni funkce modulu OutputMOdul, ktera provadi volani ostatnich funkci

@:param String input_file - obsah vvstupniho souboru
@:return String final_output - zpracovany vstupni soubor
'''


def starter_function(input_file):
    rule_list = get_position(input_file)
    final_output = get_output(rule_list, input_file)

    return final_output


'''
Funkce provadi vypis zpracovaneho souboru. V pripade, kdyz nebyl nalezen parameter --output ve vstupnich argumentech
pak script vypise zpracovany soubor do standartniho vystupu (stdout).
Pokud byl nalezen argument --output a ten je zadan spravne,pak script vypise vysledek zpracovani vstupniho souboru do 
output souboru

@:param String output - zpracovany vstupni soubor
@:return void
'''


def print_output(output):
    if Globals.br:
        output = newline_tag(output)
    # Vypis vysledku do STDOUT
    if not Globals.outputVar:
        print(output, file=sys.stdout)
    # Vypis vysledku do souboru, uvedeneho ve vstupnim argumentu --output
    else:
        print(output, file=Globals.outputFile)


'''
Funkce pridava znacku(tag) <br/> (noveho radku) do konci kazdeho radku vystupniho souboru

@:param String output - zpracovany soubor
@:return String output_with_br - zpracovany soubor, obsahujici <br/> tag na konci kazdeho radku
'''


def newline_tag(output):
    output = re.sub('\n', '<br />\n', output)

    return output
