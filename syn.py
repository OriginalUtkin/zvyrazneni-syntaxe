# Project z predmetu IPP 2016/2017s - SYN
# Author  Utkin Kirill 3BIT

import sys

from FormatModul import work_with_format
from OutputModul import starter_function, print_output
from ArgumentsModul import work_with_arguments
from GlobalModul import Globals

arguments = sys.argv[1:]

# Zpracovani vstupnich argumentu
work_with_arguments(arguments)

# Nacitani obsahu vstupniho souboru
inputFileIn = Globals.inputFile.read()

# Pokud nebyl zadan argument obsahujici cestu do formatovaciho souboru nebo vstupni formatovaci soubor je prazdny
# , pak vypise vstupni soubor do uvedeneho vystupu
if (not Globals.inputFormatVar) or (not Globals.inputFormat):
    print_output(inputFileIn)
else:  # Jinak provede zpracovani formatovaciho souboru a vytvori list, obsahujici pravidla pro kazdy regularni vyraz
    work_with_format()
    # Zpracovani vstupu
    final_output = starter_function(inputFileIn)
    # Tisk vystupu
    print_output(final_output)
