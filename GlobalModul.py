class Globals:
    # cesta do vstupniho souboru
    inputFileVar = False  # Ukazuje ze byl zadan argument --input
    inputFile = None  # Ukazatel do vstupniho souboru

    # cesta do formatovaciho souboru
    inputFormat = None  # Ukazuje ze byl zadan argument --format
    inputFormatVar = False  # Ukazatel do formatovaciho souboru

    # cesta do souboru, do ktereho ulozime vysledek zpracovani input souboru
    outputFile = None  # Ukazuje ze byl zadan argument --output
    outputVar = False  # Ukazatel do vystupniho souboru

    # Priznak nastaveni noveho radku
    br = False  # Ukazuje, ze byl zadan argument --br

    # List, ktery obsahuje vsichni ocekavane argumenty na vstupu scriptu
    validArguments = ['input=', 'output=', 'br', 'format=', 'help']
    format_file = []

    global_test = []
