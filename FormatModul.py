import re
from GlobalModul import Globals
from ErrorModul import error_print


'''
Funkce vytvari polozku listu ve tvaru 'regularni_vyraz' - 'tag'

@:param String expression - obsahuje radek ze formatovaciho souboru pro parsovani
@:return void
'''


def prepare_format_list(expression):
    # Zpracovani jednotlyvych polozek formatovaciho souboru a jejich ulozeni do listu
    list_format = re.sub(r'\^s+\t+|\s+|\n+', ' ', expression).strip(' ').split(' ')

    # iterate_param - list, obsahujici pouze parametry pro uvedeny regexp
    iterate_param = list_format.copy()
    iterate_param.__delitem__(0)

    # Pokud regularni vyraz obsahuje vice parametru nez 1 je nutne zpracovat vsichni
    for item in iterate_param:
        # Obsahuje regularni vyraz a tag, ktery obaluje vysledky
        clear_param = re.sub(r',', ' ', item).strip(' ')
        # Vytvoreni pravidla tvaru 'regular_expression' : 'tag'
        final_param = work_with_param(clear_param)
        # Zpracovani obecneho tvaru regularniho vyrazu
        final_regex = work_with_regex(list_format[0])
        # Zpracovani negovaneho tvaru regularniho vyrazu
        final_regex = negation_regex(final_regex)
        # Zajisteni, ze zpracovany regularni vyraz ve spravnem tvaru, pokud ne -> chyba cislo 4
        try:
            re.compile(final_regex)
        except re.error:
            error_print('FORMAT ERROR : Nespravny format regularniho vyrazu ve vstupnim souboru\n', 4)
        # Vytvorezni polozky tvaru 'regex' - 'tag' pro ulozeni do vysledneho listu
        rule = [final_regex, final_param]
        # Pridani vytvorene polozky do listu
        Globals.format_file.append(rule)


'''
Funkce prevadi regulari vyraz do obecheno tvaru

@:param String regex - regularni vyraz, nacteny ze fromatovaciho souboru
@:return String regex - regularni vyraz, prevedeny do spravneho tvaru
'''


def work_with_regex(regex):

    regex = re.sub('%s', '\s', regex)  # vsichni bile znaky
    regex = re.sub('%a', '.', regex)  # libovolny symbol
    regex = re.sub('%d', '\d', regex)  # cisla od 0 do 9
    regex = re.sub('%l', '[a-z]', regex)  # mala pismena  od a do z
    regex = re.sub('%L', '[A-Z]', regex)  # velka pismena od A do Z
    regex = re.sub('%w', '[a-z]|[A-Z]', regex)  # mala a velka pismena (%l|%L)
    regex = re.sub('%W', '[a-z]|[A-Z]|[0-9]', regex)  # vsechni pismena a cisla (%w|%d)
    regex = re.sub('%t', '\t', regex)  # tabulator
    regex = re.sub('%n', '\n', regex)  # novy radek

    regex = regex.replace('%.', '\.')  # .
    regex = re.sub('%\|', '\|', regex)  # |
    regex = regex.replace('%!', '\!')  # !
    regex = regex.replace('%*', '\*')  # *
    regex = regex.replace('%+', '\+')  # +
    regex = regex.replace('%(', '\(')  # (
    regex = regex.replace('%)', '\)')  # )
    regex = re.sub('%%', '\%', regex)  # %

    return regex


'''
Funkce prevadi regulari vyraz obsahujici negaci do spravneho tvaru

@:param String regex - regularni vyraz, nacteny ze fromatovaciho souboru
@:return String regex - regularni vyraz, prevedeny do spravneho tvaru
'''


def negation_regex(regex):
    regex = re.sub('!%n', r'[^\n]', regex)
    regex = re.sub('!%d', r'[^0-9]', regex)
    regex = re.sub('!%t', r'[^\t]', regex)
    regex = re.sub('!%s', r'\S', regex)
    regex = re.sub('!%L', r'[^A-Z]', regex)
    regex = re.sub('!%l', r'[^a-z]', regex)
    regex = re.sub('!%a', r'', regex)
    regex = re.sub('!%w', r'[^a-zA-Z]', regex)
    regex = re.sub('!%W', r'[^\w]', regex)
    regex = re.sub('!(.)', r'[^\1]', regex)

    return regex


'''
Funkce provadi kontrolu parametru ze formatovaciho souboru a jejich zpracovani. Na zaklade parametru
vytvari tag obalujici string pro urceny regularni vyraz, uvedeny pro tento parameter.

@:param String param_name - obsahuje parameter ze formatovaciho souboru pro regularni vyraz
@:return list param_list - list, obsahujici dvojici pripravenych tagu pro zabaleni string podle regexp
'''


def work_with_param(param_name):
    # List, obsahujici otevirajici a uzavirajici tag
    param_list = []

    # Zpracovani 'bold'
    if param_name == 'bold':
        param_list.append('<b>')
        param_list.append('</b>')

    # Zpracovani 'italic'
    elif param_name == 'italic':
        param_list.append('<i>')
        param_list.append('</i>')

    # Zpracovani 'underline'
    elif param_name == 'underline':
        param_list.append('<u>')
        param_list.append('</u>')

    # Zpracovani 'teletype'
    elif param_name == 'teletype':
        param_list.append('<tt>')
        param_list.append('</tt>')

    # Zpracovani 'size: 1 - 7'
    elif re.match(r'size:\d+', param_name):
        size = param_name.split(':')
        if int(size[1]) < 1 or int(size[1]) > 7:  # Hodnota value mimo platny rozsah -> chyba s cislem 4
            error_print('FORMAT ERROR : Hodnota value tagu  <size> mimo platny rozsah\n', 4)
        param_list.append('<font size='+size[1]+'>')
        param_list.append('</font>')

    # Zpracovani 'color : color-value'
    elif re.match('color:[a-zA-Z0-9]+', param_name):
        color = param_name.split(':')[-1]
        if len(color) != 6:
            error_print('FORMAT ERROR : Delka parametru tagu color musi se rovnat 6\n', 4)
        else:
            if len(re.findall('[0-9a-fA-F]', color)) < 6: # Color-value neodpovida spravnemu tvaru -> chyba s cislem 4
                error_print('FORMAT ERROR: Parameter tagu <color> obsahuje spatny symbol\n', 4)
            else:
                param_list.append('<font color=#'+color+'>')
                param_list.append('</font>')

    # Neocekavane pravidlo pro regularni vyraz ve formatovacim souboru -> chyba cislo 4
    else:
        error_print(4)

    return param_list

'''
Funkce pripravuje seznam, obsahujici cely formatovaci soubor. Neprijma zadne parametry , ale meni globalne promenne

@:param void 
@:return void
'''


def work_with_format():
    if Globals.inputFormatVar:
        for rule_expression in Globals.inputFormat:
            prepare_format_list(rule_expression)